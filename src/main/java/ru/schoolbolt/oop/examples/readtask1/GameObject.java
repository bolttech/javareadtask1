package ru.schoolbolt.oop.examples.readtask1;

import java.awt.Graphics;

public interface GameObject {

    void update();
    void draw(Graphics g);
    boolean isHit(int x, int y);
    boolean isOutside();

}
