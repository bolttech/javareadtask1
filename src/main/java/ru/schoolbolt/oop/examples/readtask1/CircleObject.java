package ru.schoolbolt.oop.examples.readtask1;

import java.awt.Graphics;
import java.util.Random;

public class CircleObject implements GameObject {
    private int center_x, center_y;
    private int radius;

    private int delta_x, delta_y;

    public CircleObject(int center_x, int center_y, int radius) {
        this.center_x = center_x;
        this.center_y = center_y;
        this.radius = radius;

        Random random = ObjectManager.random;

        if (center_x < Window.WIDTH / 2) {
            delta_x = random.nextInt(5) + 1;
        } else {
            delta_x = random.nextInt(5) - 5;
        }

        if (center_y < Window.HEIGHT / 2) {
            delta_y = random.nextInt(5) + 1;
        } else {
            delta_y = random.nextInt(5) - 5;
        }
    }

    @Override
    public void update() {
        center_x += delta_x;
        center_y += delta_y;
    }

    @Override
    public void draw(Graphics g) {
        g.drawOval(
                center_x - radius,
                center_y - radius,
                radius + radius,
                radius + radius
        );
    }

    @Override
    public boolean isHit(int x, int y) {
        return Math.sqrt(Math.pow(center_x - x, 2) + Math.pow(center_y - y, 2)) <= radius;
    }

    @Override
    public boolean isOutside() {
        return center_x > Window.WIDTH + 100 ||
                center_x < -100 ||
                center_y > Window.HEIGHT + 100 ||
                center_y < - 100;
    }
}
