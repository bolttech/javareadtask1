package ru.schoolbolt.oop.examples.readtask1;

import java.awt.Graphics;
import java.util.Random;

public class SquareObject implements GameObject {
    private int top_left_x, top_left_y;
    private int side_width;

    private int delta_x;
    private int delta_y;

    public SquareObject(int top_left_x, int top_left_y, int side_width) {
        this.top_left_x = top_left_x;
        this.top_left_y = top_left_y;
        this.side_width = side_width;

        Random random = ObjectManager.random;

        if (top_left_x + side_width / 2 < Window.WIDTH / 2) {
            delta_x = random.nextInt(5) + 1;
        } else {
            delta_x = random.nextInt(5) - 5;
        }

        if (top_left_y - side_width / 2 < Window.HEIGHT / 2) {
            delta_y = random.nextInt(5) + 1;
        } else {
            delta_y = random.nextInt(5) - 5;
        }
    }

    @Override
    public void update() {
        top_left_x += delta_x;
        top_left_y += delta_y;
    }

    @Override
    public void draw(Graphics g) {
        g.drawRect(top_left_x, top_left_y-side_width, side_width, side_width);
    }

    @Override
    public boolean isHit(int x, int y) {
        return x >= top_left_x &&
                x <= top_left_x + side_width &&
                y <= top_left_y &&
                y >= top_left_y - side_width;
    }

    @Override
    public boolean isOutside() {
        return top_left_x > Window.WIDTH + 100 ||
                top_left_x < -100 ||
                top_left_y > Window.HEIGHT + 100 ||
                top_left_y < -100;
    }
}
