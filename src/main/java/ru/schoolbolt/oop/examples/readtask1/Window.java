package ru.schoolbolt.oop.examples.readtask1;

import javax.swing.JFrame;
import java.awt.*;
import java.util.Timer;

public class Window extends JFrame {
    public static final int WIDTH = 800;
    public static final int HEIGHT = 600;

    private Timer timer;
    private ObjectManager objectManager;

    public Window() {
        setSize(WIDTH, HEIGHT);
        setTitle("Задача на работу с кодом (ООП)");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setResizable(false);
        setLocationRelativeTo(null);
        setVisible(true);

        objectManager = new ObjectManager(this);

        timer = new Timer();
        timer.scheduleAtFixedRate(objectManager, 33, 33);

        addMouseListener(objectManager);
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        g.setColor(Color.BLACK);
        objectManager.draw(g);
    }

}
