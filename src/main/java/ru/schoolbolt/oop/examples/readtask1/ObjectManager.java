package ru.schoolbolt.oop.examples.readtask1;

import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.ListIterator;
import java.util.Random;
import java.util.TimerTask;

public class ObjectManager extends TimerTask implements MouseListener {
    private static final int maxSpawnCounter = 30;

    private Window window;
    private ArrayList<GameObject> objects;
    private int spawnCounter;

    static Random random = new Random();

    public ObjectManager(Window window) {
        this.window = window;

        objects = new ArrayList<>();
        spawnCounter = maxSpawnCounter;
    }

    private void spawnObject() {
        if (random.nextBoolean()) {
            objects.add(spawnCircle());
        } else {
            objects.add(spawnSquare());
        }
    }

    private CircleObject spawnCircle() {
        int center_x, center_y;
        int radius = random.nextInt(20) + 5;

        if (random.nextBoolean()) {
            center_x = random.nextInt(30) - 50;
        } else {
            center_x = random.nextInt(30) + Window.WIDTH + 50;
        }

        if (random.nextBoolean()) {
            center_y = random.nextInt(30) - 50;
        } else {
            center_y = random.nextInt(30) + Window.HEIGHT + 50;
        }

        return new CircleObject(center_x, center_y, radius);
    }

    private SquareObject spawnSquare() {
        int top_left_x, top_left_y;
        int side_width = random.nextInt(20) + 5;

        if (random.nextBoolean()) {
            top_left_x = random.nextInt(30) - 50;
        } else {
            top_left_x = random.nextInt(30) + Window.WIDTH + 50;
        }

        if (random.nextBoolean()) {
            top_left_y = random.nextInt(30) - 50;
        } else {
            top_left_y = random.nextInt(30) + Window.HEIGHT + 50;
        }

        return new SquareObject(top_left_x, top_left_y, side_width);
    }

    private void spawnIfNeeded() {
        spawnCounter--;
        if (spawnCounter <= 0) {
            spawnObject();
            spawnCounter = maxSpawnCounter;
        }
    }

    private void removeIfNeeded() {
        ListIterator<GameObject> it = objects.listIterator();
        while (it.hasNext()) {
            if (it.next().isOutside()) {
                it.remove();
            }
        }
    }

    public void draw(Graphics g) {
        for (GameObject object : objects) {
            object.draw(g);
        }
    }

    @Override
    public void run() {
        spawnIfNeeded();

        for (GameObject object : objects) {
            object.update();
        }

        removeIfNeeded();

        window.invalidate();
        window.validate();
        window.repaint();
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        int x = e.getX();
        int y = e.getY();

        ListIterator<GameObject> it = objects.listIterator();
        while (it.hasNext()) {
            if (it.next().isHit(x, y)) {
                it.remove();
            }
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {
        // является частью интерфейса MouseListener,
        // поэтому обязателен к реализации, но при этом в данной задаче не нужен
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        // является частью интерфейса MouseListener,
        // поэтому обязателен к реализации, но при этом в данной задаче не нужен
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        // является частью интерфейса MouseListener,
        // поэтому обязателен к реализации, но при этом в данной задаче не нужен
    }

    @Override
    public void mouseExited(MouseEvent e) {
        // является частью интерфейса MouseListener,
        // поэтому обязателен к реализации, но при этом в данной задаче не нужен
    }
}
